### Subway Schools

My implementation of the NYC Schools app concentrates on schools that can be accessed via the New York City Subway.

The app displays each school name in an approximation of the familiar NYC subway sign typography. Subway lines that service the school are displayed below. The app is built using SwiftUI. The networking layer uses Combine.

The Subway bullets are drawn using SF Symbols images. The font is set in Helvetica. With more time for this project, it would be cool to fully and accurately spec out the MTA Sign Manual using SwiftUI.

Tapping on a school loads a details page and fetches the school's average SAT scores. The app displays these scores as subway digits for a bit of NYC flair. 🗽🚖🍕

![Schools List view](SchoolsList.png)

![School Detail view](SchoolDetail.png)
