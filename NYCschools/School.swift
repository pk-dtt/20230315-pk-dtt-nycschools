//
//  School.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/14/23.
//

import Foundation

struct School: Decodable, Identifiable, Hashable {
    var id: String { dbn }
    
    let dbn: String
    let schoolName: String
    
    let subway: String
    var subwayService: [NYCSubwayService] {
        let service = subway.split(separator: ",").map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) })
        let subwayService = service.compactMap({ NYCSubwayService(rawValue: $0) })
        return subwayService
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        
        case subway
    }
}

struct SchoolSATResults: Decodable {
    let dbn: String
    let schoolName: String
    let numTestTakers: String
    let averageScoreReading: String
    let averageScoreMath: String
    let averageScoreWriting: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numTestTakers = "num_of_sat_test_takers"
        case averageScoreReading = "sat_critical_reading_avg_score"
        case averageScoreMath = "sat_math_avg_score"
        case averageScoreWriting = "sat_writing_avg_score"
    }
}
