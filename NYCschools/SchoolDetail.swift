//
//  SchoolDetail.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/15/23.
//

import Foundation
import SwiftUI
import Combine

class SchoolDetailViewModel: ObservableObject {
    
    var cancellable: AnyCancellable?
    
    @Published var school: School
    @Published var satResults: SchoolSATResults?
    @Published var didFetch: Bool = false
    
    init(school: School, satResults: SchoolSATResults? = nil) {
        self.school = school
        self.satResults = satResults
    }
    
    func fetchSATResults() {
        self.cancellable = NYCOpenDataWebService.satResults(for: school)
            .mapError({ (error) -> Error in
                self.didFetch = true
                return error
            })
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { testResults in
                self.didFetch = true
                self.satResults = testResults.first
            })
    }
    
}

struct SchoolDetail: View {
    
    @ObservedObject var viewModel: SchoolDetailViewModel
    
    var body: some View {
        List {
            VStack(alignment: .leading) {
                Text(viewModel.school.schoolName)
                    .font(.subwaySignFont)
                HStack(spacing: 4) {
                    ForEach(viewModel.school.subwayService) {
                        $0.image
                            .font(.subwaySignFont)
                    }
                    Spacer()
                }
                .padding([.bottom], 10)
                
                HStack {
                    Spacer()
                    Text("Average SAT Scores").font(.subwaySignFont)
                }
                .padding([.bottom], 10)
                
                // MATH
                HStack {
                    Text("Math").font(.subwaySignFont)
                    Spacer()
                    if let score = viewModel.satResults?.averageScoreMath {
                        ForEach(score.subwayServices, id: \.self) { service in
                            service.image.font(.subwaySignFont).padding([.leading, .trailing], -4)
                        }
                    } else {
                        progressOrError
                    }
                }
                .padding([.bottom], 8)
                
                // READING
                HStack {
                    Text("Reading").font(.subwaySignFont)
                    Spacer()
                    if let score = viewModel.satResults?.averageScoreReading {
                        ForEach(score.subwayServices, id: \.self) { service in
                            service.image.font(.subwaySignFont).padding([.leading, .trailing], -4)
                        }
                    } else {
                        progressOrError
                    }
                }
                .padding([.bottom], 8)
                
                // WRITING
                HStack {
                    Text("Writing").font(.subwaySignFont)
                    Spacer()
                    if let score = viewModel.satResults?.averageScoreWriting {
                        ForEach(score.subwayServices, id: \.self) { service in
                            service.image.font(.subwaySignFont).padding([.leading, .trailing], -4)
                        }
                    } else {
                        progressOrError
                    }
                }
                .padding([.bottom], 8)
            }
            .padding()
            .onAppear() {
                viewModel.fetchSATResults()
            }
            .contentShape(Rectangle())
            .onTapGesture {
                if viewModel.satResults == nil {
                    self.viewModel.didFetch = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        // small delay so that we can actually see the spinner
                        viewModel.fetchSATResults()
                    }
                }
            }
        }
    }
    
    @ViewBuilder var progressOrError: some View {
        if !viewModel.didFetch {
            ProgressView().progressViewStyle(CircularProgressViewStyle())
        } else {
            Image(systemName: "arrow.down.right.square").font(.subwaySignFont)
        }
    }
}
