//
//  NYCOpenData.swift
//  NYCschools
//
//  Created by Kamb, Peter on 3/14/23.
//

import Foundation
import Combine

struct NYCOpenDataWebService {
    
    // Minimum web service class to handle the NYC OpenData networking
    //
    // This would be expanded into a more full-featured networking class.
    
    static var baseURL = "data.cityofnewyork.us"
    
    enum APIRequests {
        case fetchSchools
        case satResults
        
        var endpointPath: String {
            switch self {
            case .fetchSchools: return "/resource/s3k6-pzi2.json"
            case .satResults:   return "/resource/f9bf-2cp4.json"
            }
        }
        
        var urlComponents: URLComponents {
            var components = URLComponents()
            components.scheme = "https"
            components.host = NYCOpenDataWebService.baseURL
            components.path = endpointPath
            return components
        }
    }
    
    static func fetchSchools(limit: UInt? = nil) -> AnyPublisher<[School], Error> {
        var urlComponents = APIRequests.fetchSchools.urlComponents
        
        if let limit = limit {
            urlComponents.queryItems = [
                URLQueryItem(name: "$limit", value: "\(limit)"),
            ]
        }
        guard let url = urlComponents.url else { fatalError("Error building URL components. Better handle errors like!") }
        
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .mapError { $0 as Error }
            .map(\.data)
            .decode(type: [School].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    static func satResults(for school: School) -> AnyPublisher<[SchoolSATResults], Error> {
        var urlComponents = APIRequests.satResults.urlComponents
        urlComponents.queryItems = [
            URLQueryItem(name: "dbn", value: school.dbn),
        ]
        guard let url = urlComponents.url else { fatalError("Error building URL components. Better handle errors like!") }
        
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .mapError { $0 as Error }
            .map(\.data)
            .decode(type: [SchoolSATResults].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
}
